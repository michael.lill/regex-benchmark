#!/bin/sh

g++ regex-benchmark.cc -std=c++11 -isystem benchmark/include -I re2 -Lhyperscan_build/lib/ -lhs -Lbenchmark_build/src -lbenchmark -Lre2/obj -lre2 -lpthread -Lregex/target/release -lrure -o bin/regex-benchmark

export LD_LIBRARY_PATH=regex/target/release/

./bin/regex-benchmark | tee `date -I`.result 