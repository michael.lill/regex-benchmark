#include <string>
#include <iostream>
#include <unordered_map>
#include <cmath>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <benchmark/benchmark.h>
#include "hyperscan/src/hs.h"
#include <re2/re2.h>
#include "regex/regex-capi/include/rure.h"

using namespace std;

static int eventHandler(unsigned int id, unsigned long long from,
                        unsigned long long to, unsigned int flags, void *ctx)
{
  printf("Match from %llu to %llu\n", from, to);
  return 0;
}

static void HyperscanExcludingCompilationOfRegex(benchmark::State &state)
{
  char *pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";
  hs_database_t *database;
  hs_compile_error_t *compile_err;
  if (hs_compile(pattern, HS_FLAG_DOTALL, HS_MODE_BLOCK, NULL, &database,
                 &compile_err) != HS_SUCCESS)
  {
    fprintf(stderr, "ERROR: Unable to compile pattern \"%s\": %s\n",
            pattern, compile_err->message);
    hs_free_compile_error(compile_err);
    exit(1);
  }

  hs_scratch_t *scratch = NULL;
  if (hs_alloc_scratch(database, &scratch) != HS_SUCCESS)
  {
    fprintf(stderr, "ERROR: Unable to allocate scratch space. Exiting.\n");
    hs_free_database(database);
    exit(1);
  }

  std::string inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  for (auto _ : state)
  {
    if (hs_scan(database, &inputData[0], strnlen(&inputData[0], 1000000000000LL), 0, scratch, NULL,
                pattern) != HS_SUCCESS)
    {
      fprintf(stderr, "ERROR: Unable to scan input buffer. Exiting.\n");
      hs_free_database(database);
      exit(1);
    }
  }
}

static void HyperscanIncludingCompilationOfRegex(benchmark::State &state)
{
  char *pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";

  std::string inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  for (auto _ : state)
  {

    hs_database_t *database;
    hs_compile_error_t *compile_err;
    if (hs_compile(pattern, HS_FLAG_DOTALL, HS_MODE_BLOCK, NULL, &database,
                   &compile_err) != HS_SUCCESS)
    {
      fprintf(stderr, "ERROR: Unable to compile pattern \"%s\": %s\n",
              pattern, compile_err->message);
      hs_free_compile_error(compile_err);
      exit(1);
    }

    hs_scratch_t *scratch = NULL;
    if (hs_alloc_scratch(database, &scratch) != HS_SUCCESS)
    {
      fprintf(stderr, "ERROR: Unable to allocate scratch space. Exiting.\n");
      hs_free_database(database);
      exit(1);
    }
    if (hs_scan(database, &inputData[0], strnlen(&inputData[0], 1000000000000LL), 0, scratch, NULL,
                pattern) != HS_SUCCESS)
    {
      fprintf(stderr, "ERROR: Unable to scan input buffer. Exiting.\n");
      hs_free_database(database);
      exit(1);
    }
  }
}

static void Re2FullMatch(benchmark::State &state)
{
  std::string pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";

  std::string inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  for (auto _ : state)
  {
    RE2::FullMatch(inputData, pattern);
  }
}

static void Re2PartialMatch(benchmark::State &state)
{
  std::string pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";

  std::string inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  for (auto _ : state)
  {
    RE2::PartialMatch(inputData, pattern);
  }
}

static void RustRegexIncludingCompilationOfRegex(benchmark::State &state)
{
  /*
     * Compile the regular expression. A more convenient routine,
     * rure_compile_must, is also available, which will abort the process if
     * and print an error message to stderr if the regex compilation fails.
     * We show the full gory details here as an example.
     */
  const char *pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";
  size_t pattern_len = strlen(pattern);

  const char *inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  rure_error *err = rure_error_new();
  for (auto _ : state)
  {
    rure *re = rure_compile((const uint8_t *)pattern, pattern_len,
                            RURE_FLAG_UNICODE | RURE_FLAG_CASEI, NULL, err);
    if (NULL == re)
    {
      /* A null regex means compilation failed and an error exists. */
      printf("compilation of %s failed: %s\n",
             pattern, rure_error_message(err));
      rure_error_free(err);
      exit(1);
    }

    /*
     * Create an iterator to find all successive non-overlapping matches.
     * For each match, we extract the location of the capturing group.
     */
    rure_match group0 = {0};
    rure_match group1 = {0};
    rure_captures *caps = rure_captures_new(re);
    rure_iter *it = rure_iter_new(re);

    while (rure_iter_next_captures(it, (const uint8_t *)inputData, strlen(inputData), caps))
    {
      /*
         * Get the location of the full match and the capturing group.
         * We know that both accesses are successful since the body of the
         * loop only executes if there is a match and both capture groups
         * must match in order for the entire regex to match.
         *
         * N.B. The zeroth group corresponds to the full match of the regex.
         */
      rure_captures_at(caps, 0, &group0);
      rure_captures_at(caps, 1, &group1);
    }
    rure_captures_free(caps);
    rure_iter_free(it);
    rure_free(re);
  }
  rure_error_free(err);
}

static void RustRegexExcludingCompilationOfRegex(benchmark::State &state)
{
  /*
     * Compile the regular expression. A more convenient routine,
     * rure_compile_must, is also available, which will abort the process if
     * and print an error message to stderr if the regex compilation fails.
     * We show the full gory details here as an example.
     */
  const char *pattern = "#(?:[a-f\\d]{3}){1,2}\b|rgb\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){2}\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)|\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%(?:\\s*,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%){2})\\s*\\)|hsl\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2}\\)|(?:rgba\\((?:(?:\\s*0*(?:25[0-5]|2[0-4]\\d|1?\\d?\\d)\\s*,){3}|(?:\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*,){3})|hsla\\(\\s*0*(?:360|3[0-5]\\d|[12]?\\d?\\d)\\s*(?:,\\s*0*(?:100(?:\\.0+)?|\\d?\\d(?:\\.\\d+)?)%\\s*){2},)\\s*0*(?:1|0(?:\\.\\d+)?)\\s*\\)";
  size_t pattern_len = strlen(pattern);

  const char *inputData = "RegExr was created by gskinner.com, and is proudly hosted by Media Temple. Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode. The side bar includes a Cheatsheet, full rgb(255,255,255)Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns. Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.";

  rure_error *err = rure_error_new();

  rure *re = rure_compile((const uint8_t *)pattern, pattern_len,
                          RURE_FLAG_UNICODE | RURE_FLAG_CASEI, NULL, err);
  if (NULL == re)
  {
    /* A null regex means compilation failed and an error exists. */
    printf("compilation of %s failed: %s\n",
           pattern, rure_error_message(err));
    rure_error_free(err);
    exit(1);
  }
  rure_match group0 = {0};
  rure_captures *caps = rure_captures_new(re);
  rure_iter *it = rure_iter_new(re);
  for (auto _ : state)
  {
    /*
     * Create an iterator to find all successive non-overlapping matches.
     * For each match, we extract the location of the capturing group.
     */

    while (rure_iter_next_captures(it, (const uint8_t *)inputData, strlen(inputData), caps))
    {
      /*
         * Get the location of the full match and the capturing group.
         * We know that both accesses are successful since the body of the
         * loop only executes if there is a match and both capture groups
         * must match in order for the entire regex to match.
         *
         * N.B. The zeroth group corresponds to the full match of the regex.
         */
      rure_captures_at(caps, 0, &group0);
    }
  }
  rure_captures_free(caps);
  rure_iter_free(it);
  rure_free(re);
  rure_error_free(err);
}

BENCHMARK(HyperscanIncludingCompilationOfRegex);
BENCHMARK(HyperscanExcludingCompilationOfRegex);
BENCHMARK(Re2FullMatch);
BENCHMARK(Re2PartialMatch);
BENCHMARK(RustRegexIncludingCompilationOfRegex);
BENCHMARK(RustRegexExcludingCompilationOfRegex);

BENCHMARK_MAIN();