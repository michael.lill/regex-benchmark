#!/bin/sh
cd hyperscan_build/
cmake ../hyperscan/
make -j 8

cd ../benchmark_build/
cmake -DCMAKE_BUILD_TYPE=Release ../benchmark/
make -j 8

cd ../re2/
make -j 8

cd ../regex/
cargo build --release

cd ../regex/regex-capi/
cargo build --release
