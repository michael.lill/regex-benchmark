# This is a playground built for benchmarking some regex libraries

> Note that my knowledge of C, C++, Rust is quite limited. It was done only for self-educational purposes and fun. 

> Also note that these results do not have much meaning (yet) since it's a comparison of apples and oranges.